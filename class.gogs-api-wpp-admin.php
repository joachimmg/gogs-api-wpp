<?php

class Gogs_API_Plugin_Admin {
    private static $init = false;

    public static function init() {
        if (self::$init)
            return;

        self::init_hooks();
    }

    private static function init_hooks() {
        self::$init = true;
        add_action("admin_init", array(__CLASS__, "admin_init"), 5);
        add_action("admin_menu", array(__CLASS__, "admin_menu"), 5);
    }

    public static function admin_init() {

        load_plugin_textdomain(GAWPP_TEXT_DOMAIN);

        register_setting("gawpp", "gawpp_url", function(string $url) {

            $rurl = esc_url_raw($url);

            if (empty($rurl))
                add_settings_error(
                    "gawpp_messages",
                    "gawpp_message",
                    esc_html(
                        sprintf(__("Url %s is invalid", GAWPP_TEXT_DOMAIN), 
                            $url
                        )
                    ),
                    "error"
                );

            if (($pos = strrpos($rurl, "/api/v")))
                $rurl = substr($rurl, 0, $pos);

            return empty($rurl = rtrim($rurl, "/")) ? false : $rurl;
        });

        register_setting("gawpp", "gawpp_token", function(string $token) {

            if(preg_match("/^[0-9a-f]{40}$/i", $token))
                return $token;

            add_settings_error(
                "gawpp_messages",
                "gawpp_message",
                esc_html(
                    sprintf(__("The token \"%s\" is not a valid sha1-hash", GAWPP_TEXT_DOMAIN), $token)
                ),
                "error"
            );

            return false;
        });

        register_setting("gawpp", "gawpp_apiv", function(string $apiv) {
            $apiv = explode(":", $apiv);

            if (count($apiv) == 2 && is_numeric($apiv[0])) {
                $idx = intval($apiv[0]);

                if ($idx >= 0) {
                    $opts = Gogs_API_Plugin::get_api_supported();

                    if (isset($opts[$idx]) && $opts[$idx]["path"] == $apiv[1])
                        return $opts[$idx]["path"];
                }
            }

            add_settings_error(
                "gawpp_messages",
                "gawpp_message",
                esc_html(
                    sprintf(__("The api version selected \"%s\" is not supported", GAWPP_TEXT_DOMAIN), join(":", $apiv))
                ),
                "error"
            );

            return false;
        });

        register_setting("gawpp", "gawpp_usr", function(string $user){

            if (empty(trim($user)))
                return false;

            if (preg_match("/[a-z0-9\\_\\-i\\.]+$/i", $user))
                return $user;

            add_settings_error(
                "gawpp_messages",
                "gawpp_message",
                esc_html(
                    __("The username must be valid alpha or numeric or dash(-_) or dot characters", GAWPP_TEXT_DOMAIN)
                ),
                "error"
            );
        });

        register_setting("gawpp", "gawpp_credit", function(string $opt){

            if (empty(trim($opt)))
                return "yes";

            if (in_array($opt, array("yes", "no")))
                return $opt;

            return "yes";
        });

        add_settings_section(
            "gawpp_config",
            __("Required", GAWPP_TEXT_DOMAIN),
            function(array $args) {
                echo sprintf(
                    '<p id="%s">%s</p>', 
                    esc_attr($args["id"]), 
                    esc_html__(
                        "Information about your Gogs-site that Gogs API WPPlugin need to know to make queries. It's recommended to fill in these fields first, to ensure you have entered the required fields correctly before filling out the optional fields.", 
                        GAWPP_TEXT_DOMAIN
                    )
                );
            },
            "gawpp"
        );

        $field = function(array $args) {
            $options = get_option("gawpp_" . $args["id"]);
            echo sprintf(
                '<input type="text" id="%1$s" name="gawpp_%1$s" class="%2$s"%3$s%4$s%5$s/>',
                esc_attr($args["label_for"]),
                esc_attr($args["class"]),
                (isset($args["placeholder"]) && !empty($args["placeholder"]) ?
                    sprintf(' placeholder="%s"', esc_attr($args["placeholder"])) : null
                ),
                (isset($args["data"]) && !empty($args["data"]) ?
                    sprintf(' data-custom="%s"', esc_attr($args["data"])) : null
                ),
                (($value = Gogs_API_Plugin::get_option($args["label_for"])) != false ?
                    sprintf(' value="%s"', $value) : null
                )
            );
        };

        add_settings_field(
            "gawpp_url",
            __("Gogs URL", GAWPP_TEXT_DOMAIN),
            $field,
            "gawpp",
            "gawpp_config",
            array(
                'label_for' => "url",
                'class' => "regular-text code",
                'data' => "",
                'placeholder' => 'https://git.domain.tld'
            ),
            trim(GAWPP_GOGS_LOGO)
        );

        add_settings_field(
            "gawpp_token",
            __("Application token", GAWPP_TEXT_DOMAIN),
            $field,
            "gawpp",
            "gawpp_config",
            array(
                'label_for' => "token",
                'class' => "regular-text code",
                'data' => "",
                'placeholder' => 'e.g c7397d798d241d6462ac992680fa6648d83132ab'
            )
        );

        add_settings_field(
            "gawpp_apiv",
            __("API version", GAWPP_TEXT_DOMAIN),
            function (array $args) {
                $selected = Gogs_API_Plugin::get_api_path();
                echo sprintf('<select id="%1$s" name="gawpp_%1$s" class="%2$s">%3$s</select>',
                    $args["label_for"],
                    $args["class"],
                    (function() use ($args, $selected) {
                        $opts = array();
                        foreach(Gogs_API_Plugin::get_api_supported() as $key => $opt)
                            $opts[] = sprintf('<option value="%d:%s"%s>API %s: %s</value>', 
                                (int)$key, 
                                esc_attr($opt["path"]),
                                $selected == $opt["path"] ? " selected" : "",
                                esc_html($opt["name"]),
                                esc_html($opt["path"]) 
                            );
                        return join($opts);
                    })()
                );
            },
            "gawpp",
            "gawpp_config",
            array(
                'label_for' => "apiv",
                'class' => "regular-text code"
            )
        );

        add_settings_section(
            "gawpp_config_optional",
            __("Optional", GAWPP_TEXT_DOMAIN),
            function(array $args) {
                echo sprintf(
                    '<p id="%s">%s</p>', 
                    esc_attr($args["id"]), 
                    esc_html__(
                        "Information you can add to change the experience and/or behaviour, but you should first fill out (and save!) the required fields to ensure you have entered them correctly. Otherwise it can be tricky to find out which field you have entered incorrectly, e.g if the username is unknown for your Gogs-site, Gogs API will return \"Not found\" - which is the same status code if the API URL is wrong.", 
                        GAWPP_TEXT_DOMAIN
                    )
                );
            },
            "gawpp"
        );

        add_settings_field(
            "gawpp_usr",
            __("Gogs user", GAWPP_TEXT_DOMAIN),
            $field,
            "gawpp",
            "gawpp_config_optional",
            array(
                'label_for' => "usr",
                'class' => "regular-text code",
                'data' => "",
                'placeholder' => 'optional'
            )
        );

        add_settings_field(
            "gawpp_credit",
            __("Allow credit", GAWPP_TEXT_DOMAIN),
            function(array $args) {

                $selected = Gogs_API_Plugin::get_option($args["label_for"]);

                $inputs = array();
                foreach(array("yes", "no") as $opt)
                    $inputs[] = sprintf('<input type="radio" name="gawpp_%1$s" id="%2$s_%1$s" value="%2$s"%3$s%4$s/><label for="%2$s_%1$s">%5$s</label>',
                        $args["label_for"],
                        $opt,
                        !empty($args["class"]) ? sprintf(' class="%s"', esc_attr($args["class"])) : null,
                        (function() use ($selected, $opt) {
                            if (empty($selected) && $opt == "yes")
                                return ' checked';
                            else if ($selected == $opt)
                                return ' checked';
                            return null;
                        })(),
                        ucfirst(esc_html__($opt, GAWPP_TEXT_DOMAIN)) . ", " . (
                            $opt == "yes" ?
                            __("of course I'll give you credit!", GAWPP_TEXT_DOMAIN) :
                            sprintf('%s <a href="https://www.paypal.me/joachimmgok/86nok">%s</a>!',
                                __("but I'll consider to", GAWPP_TEXT_DOMAIN),
                                __("buy you a beer", GAWP_TEXT_DOMAIN)
                            )
                        )
                    );
                echo '<p>' . join("<br />", $inputs) . "</p><p class=\"description\">Similar to the credit shown below.</p>";
            },
            "gawpp",
            "gawpp_config_optional",
            array(
                'label_for' => "credit",
                'class' => "regular-text code",
            )
        );
    }

    public static function admin_menu() {

        $hook = add_menu_page(
            "Gogs API", 
            "Gogs API options", 
            "manage_options", 
            "gawpp", 
            array(__CLASS__, "display_page"),
            GAWPP_GOGS_LOGO
        );

        add_action("load-$hook", array(__CLASS__, "display_help"));

    }

	public static function get_page_url($action = 'config') {

		$args = array('page' => 'gawpp');

		return add_query_arg( $args, admin_url('admin.php'));
	}

    public static function display_page() {
        include GAWPP_DIR . "views/config.php";
    }

    public static function display_help() {

        $screen = get_current_screen();

        if (current_user_can("manage_options")) {
            $screen->add_help_tab(
                array(
                    "id" => "overview",
                    "title" => __("Overview", GAWPP_TEXT_DOMAIN),
                    "content" => 
                        "<p><strong>" . esc_html(__("Gogs API WPP", GAWPP_TEXT_DOMAIN)) . "</strong></p>" .
                        "<p>Gogs API Wordpress Plugin lets you query you Gogs installation and show this data in you blog.</p>"
                )
            );

            $screen->add_help_tab(
                array(
                    "id" => "setup",
                    "title" => __("Setup", GAWPP_TEXT_DOMAIN),
                    "content" =>
                    "<p><strong>" . esc_html(__("Gogs API WPP Setup", GAWPP_TEXT_DOMAIN)) . "</strong></p>" . <<<EOF
<p>To setup Gogs API WPP you must have an application token generated from your Gogs-account.</p>
<ol>
    <li>Generate application token from you Gogs settings and store it securely. You will only be able to read it once!</li>
    <li>Ensure your Gogs installation supports Gogs API v1.</li>
    <li>Enter the URL for you Gogs page, without the /api/v1/ postfix - we'll add it for you, and your application token.</li>
    <li>If you want to list things for another user than the owner of the application token, specify the username, but you can leave it blank to operate on the authorized user. <strong>Please note</strong> that only public content (repos, organization/team memberships etc) will be listet on a specified user. <strong>This also if applies to your user, if you specify the username</strong>.</li>
</ol>
EOF
                )
            );
        } 
    }

    public static function notify_setup_missing(string $resp = null) {
        add_action("admin_notices", function() use ($resp) {
            $screen = get_current_screen();
            if ($screen->id == "toplevel_page_gawpp")
                return;

            echo sprintf('<div class="notice notice-warning"><p class="wp-clearfix"><strong>%1$s</strong> %2$s%5$s <a href="%3$s" title="%4$s" class="button button-primary alignright">%4$s</a></p></div>',
                __("Almost done!", GAWPP_TEXT_DOMAIN),
                __("Gogs API Wordpress Plugin needs additional setup to work.", GAWPP_TEXT_DOMAIN),
                self::get_page_url("config"),
                __("Complete setup", GAWPP_TEXT_DOMAIN),
                !empty($resp) ? (
                    " <em>" . esc_html($resp) . "</em>"
                ) : null
            );
        });
    }
}
?>
