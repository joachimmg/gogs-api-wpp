<?php

require GAWPP_DIR . "gogs-php-api-client/src/gpac.php";

class Gogs_API_Plugin { 

    private static $versions_supported = array(
         array("name" => "v1", "path" => "/api/v1")
    );

    private static $client = false;
    private static $user = false;

    public static function init() {
        if (!self::$client && !empty(self::get_url()) && !empty(self::get_token())) {
            if(($resp = self::verify()) !== true)
                Gogs_API_Plugin_Admin::notify_setup_missing($resp);
        } else {
            Gogs_API_Plugin_Admin::notify_setup_missing();
        }
    }

    public static function user() {
        return self::$user;
    }

    public static function get_url() {
        return self::get_option("url", false); 
    }

    public static function get_token() {
        return self::get_option("token", false);
    }

    public static function get_user() {
        return self::get_option("usr", false);
    }

    public static function get_api_path() {
        return self::get_option("apiv", self::$versions_supported[0]["path"]);
    }

    public static function get_credit(bool $force = false, bool $formatted = false) {
        if (($cred = self::get_option("credit", "yes")) && ($force || !$cred || $cred == 'yes')) {
            $data = get_plugin_data(GAWPP_DIR . "gogs-api-wpp.php");
            $credit = array(
                "plugin" => array(
                    "name" => esc_html($data["Name"]),
                    "uri" => esc_attr($data["PluginURI"]),
                    "version" => esc_html($data["Version"]),
                    "fyear" => 2017,
                    "nyear" => (int)date("Y"),
                ), "author" => array(
                    "name" => esc_html($data["AuthorName"]),
                    "uri" => esc_attr($data["AuthorURI"])
                )
            );

            if ($formatted)
                return sprintf('<a href="%1$s" title="%2$s">%2$s</a> &copy; %3$s <a href="%4$s" title="%5$s">%5$s</a>',
                    $credit["plugin"]["uri"],
                    $credit["plugin"]["name"],
                    (function() use ($credit) {
                        if ($credit["plugin"]["fyear"] != $credit["plugin"]["nyear"])
                            return sprintf("%d - %d", $credit["plugin"]["fyear"], $credit["plugin"]["nyear"]);

                        return sprintf("%d", $credit["plugin"]["fyear"]);
                    })(),
                    $credit["author"]["uri"],
                    $credit["author"]["name"]
                );

            return $credit;
        }
        return null;
    }

    public static function get_api_supported() {
        return self::$versions_supported;
    }

    public static function get_option(string $name, $default = false) {
        return !empty($ret = get_option("gawpp_" . $name, $default)) ? $ret : $default;
    }

    private static function set_option(string $name, string $value, string $autol = 'yes') {
        return update_option("gawpp_" . $name, $value, $autol);
    }

    private static function del_option(string $name) {
        return delete_option("gawpp_" . $name);
    }

    public static function verify() {
        self::$client = new Gogs\API\Client(
            self::get_url() . self::get_api_path(),
            self::get_token()
        );

        try {
            self::$user = self::$client->user(self::get_user())->load();
        } catch (Gogs\Lib\Curl\Exception\HTTPUnexpectedResponse $e) {
            return (string)$e->getMessage();
        }

        return true;
    }

    public static function on_activate() {
        register_setting("gawpp", "gawpp_url");
        register_setting("gawpp", "gawpp_token");
        register_setting("gawpp", "gawpp_usr");
    }

    public static function on_deactivate() {
        self::del_option("url");
        self::del_option("token");
        self::del_option("usr");
        unregister_setting("gawpp", "gawpp_url");
        unregister_setting("gawpp", "gawpp_token");
        unregister_setting("gawpp", "gawpp_usr");
    }
}
