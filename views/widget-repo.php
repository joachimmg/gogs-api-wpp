<dl class="row">
    <dt class="name col-sm-3">
        <a href="%1$s" title="%2$s">%2$s</a>
    </dt>
    <dd class="details col-sm-9">
        <span>
            <i class="fa fa-lock%6$s"></i>
            <i class="fa fa-star"></i> %4$s
            <i class="fa fa-code-fork" aria-hidden="true">%5$s</i>
            %7$s
        </span>
    </dd>
    <dd class="description col-sm-9">
        %3$s
    </dd>
</dl>
