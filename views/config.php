<?php 
gawpp_access();

if (!gawpp_manage()) return;

if (isset($_GET["settings-updated"])) {
    $errors = count(get_settings_errors("gawpp_messages"));

    add_settings_error(
        "gawpp_messages", 
        "gawpp_message", 
        sprintf(
            esc_html__("Settings saved%s!", GAWPP_TEXT_DOMAIN), 
            (function($errors) {
                switch ($errors) {
                case 0:
                    return null;
                default:
                    return sprintf(
                        __(", but with %d %s", GAWPP_TEXT_DOMAIN), 
                        $errors, 
                        _n("error", "errors", $errors, GAWPP_TEXT_DOMAIN)
                    );
                }
            })($errors)
        ),
        "updated"
    );

    if ($errors == 0) {
        if (!empty($err = Gogs_API_Plugin::verify()) && is_string($err))
            add_settings_error(
                "gawpp_messages", 
                "gawpp_messages", 
                sprintf("%s", esc_html($err)),
                "error"
            );
    }

}

settings_errors("gawpp_messages");

?>
<div class="wrap">
    <h1><?=esc_html(get_admin_page_title());?></h1>
    <form action="options.php" method="post">
        <?php 
            settings_fields("gawpp");
            do_settings_sections("gawpp");
            submit_button("Save configuration");
        ?>
    </form>
    <center><?=Gogs_API_Plugin::get_credit(true, true);?></center>
</div>
