<?php
/**
 * @package Gogs API WPPlugin
 */

/*
Plugin Name: Gogs API WPPlugin
Plugin URI: https://git.giaever.org/joachimmg/gogs-api-wpp
Description: Show your Gogs (GO git service)[https://gogs.io] repositories, activity etc in you blog.
Version: 0.0.1
Author: Joachim Marthinsen Giæver
Author URI: https://joachimmg.no
License: GPLv3
Text Domain: gogs-api-wpp
*/

function gawpp_access() {
    defined("ABSPATH") or die("No access");
}

function gawpp_manage() {
    return current_user_can("manage_options");
}

gawpp_access();

define("GAWPP_VERSION", "0.0.1");
define("GAWPP_WP_MIN_VERSION", "4.8.2");
define("GAWPP_DIR", plugin_dir_path(__FILE__));
define("GAWPP_TEXT_DOMAIN", "gogs-api-wpp");
define("GAWPP_GOGS_LOGO", "data:image/svg+xml;base64," . base64_encode(<<<EOF
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 67.733332 67.733335" enable-background="new"><defs><clipPath id="d"><path d="M33.867 243.433c-10.88 0-19.7 8.82-19.7 19.7 0 10.88 8.82 19.701 19.7 19.701a19.7 19.7 0 0 0 15.688-7.846l.018.011.103-.177a19.7 19.7 0 0 0 2.299-3.981l-12.388-7.153a5.747 5.747 0 1 0-5.719 5.193 5.742 5.742 0 0 0 3.294-1.047l7.438 4.294a14.034 14.034 0 0 1-10.732 5.04c-7.751 0-14.035-6.283-14.035-14.034S26.117 249.1 33.868 249.1c1.644.009 3.274.307 4.815.881.433.231.919.352 1.413.352 1.62 0 2.932-1.272 2.932-2.842-.001-1.016-.711-1.963-1.62-2.47 0 0-.46-.226-.62-.288a19.445 19.445 0 0 0-6.92-1.3z" fill="#000" stroke="#000" stroke-width=".661" stroke-linecap="round"/></clipPath><linearGradient xlink:href="#a" id="c" x1="58.605" y1="304.011" x2="7.144" y2="224.769" gradientUnits="userSpaceOnUse"/><filter id="e" x="-.002" width="1.005" y="-.002" height="1.005" color-interpolation-filters="sRGB"><feGaussianBlur stdDeviation=".039"/></filter><filter id="b" x="-.002" width="1.005" y="-.002" height="1.005" color-interpolation-filters="sRGB"><feGaussianBlur stdDeviation=".064"/></filter></defs><g stroke-width=".661" stroke-linecap="round"><path d="M27.626 230.47l-1.796 7.392a26.55 26.55 0 0 0-9.96 5.785l-7.244-2.123-6.168 10.682 5.493 5.242a26.55 26.55 0 0 0-.635 5.685A26.55 26.55 0 0 0 8 268.97l-5.469 5.217L8.7 284.87l7.3-2.14a26.55 26.55 0 0 0 9.998 5.76l1.776 7.307h12.334l1.796-7.391a26.55 26.55 0 0 0 9.96-5.786l7.244 2.123 6.168-10.682-5.492-5.241a26.55 26.55 0 0 0 .634-5.687 26.55 26.55 0 0 0-.683-5.835l5.468-5.219-6.168-10.682-7.3 2.14a26.55 26.55 0 0 0-9.996-5.759l-1.776-7.309z" fill="url(#c)" stroke="#000" transform="translate(0 -229.267)"/><path d="M33.949 243.795c-10.88 0-19.706 8.974-19.7 19.7.004 10.728 8.82 19.702 19.7 19.701 6.17-.014 11.976-2.942 15.688-7.845l.018.01.103-.177a19.824 19.824 0 0 0 2.298-3.98l-12.388-7.154c.018-.174.028-.378.028-.554-.005-3.007-2.573-5.746-5.747-5.746-3.173 0-5.752 2.74-5.746 5.746.005 3.008 2.573 5.747 5.746 5.747 1.18-.002 2.331-.399 3.295-1.047l7.438 4.295c-2.655 3.113-6.586 5.028-10.733 5.04-7.75 0-14.026-6.53-14.034-14.035-.008-7.504 6.283-14.034 14.034-14.034 1.645.01 3.274.336 4.816.881.433.228.918.38 1.413.353 1.622-.087 2.929-1.172 2.932-2.843 0-1.081-.711-1.966-1.62-2.469 0 0-.46-.228-.621-.289a20.077 20.077 0 0 0-6.92-1.3z" clip-path="url(#d)" fill="#000" stroke="#000" filter="url(#e)" transform="translate(0 -229.267)"/><path d="M33.867 14.166c-10.88 0-19.7 8.82-19.7 19.7 0 10.881 8.82 19.701 19.7 19.701a19.7 19.7 0 0 0 15.688-7.845l.018.01.103-.177a19.7 19.7 0 0 0 2.298-3.981l-12.387-7.153a5.747 5.747 0 1 0-5.719 5.193 5.742 5.742 0 0 0 3.294-1.047l7.438 4.295a14.034 14.034 0 0 1-10.732 5.04c-7.751 0-14.035-6.284-14.035-14.035 0-7.75 6.284-14.034 14.035-14.034 1.644.009 3.274.308 4.815.881.433.231.919.353 1.413.353 1.62 0 2.932-1.273 2.932-2.843-.001-1.016-.711-1.963-1.62-2.469 0 0-.46-.226-.62-.289a19.445 19.445 0 0 0-6.92-1.3z" fill="#000" stroke="#000"/></g></svg>
EOF
));

require_once GAWPP_DIR . "class.gogs-api-wpp.php";

register_activation_hook(__FILE__, array("Gogs_API_Plugin", "on_activation"));
register_deactivation_hook(__FILE__, array("Gogs_API_Plugin", "on_deactivation"));

add_action("init", array("Gogs_API_Plugin", "init"));

require_once GAWPP_DIR . "class.gogs-api-wpp-widget.php";
add_action("widgets_init", function() {
    register_widget("Gogs_API_Plugin_Widget");
});

if (is_admin()) {
    require_once GAWPP_DIR . "class.gogs-api-wpp-admin.php";
    add_action("init", array("Gogs_API_Plugin_Admin", "init"));
}
