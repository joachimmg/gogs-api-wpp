<?php

class TimeAgo extends DateInterval {

    public static function set(DateInterval $obj) {
        return unserialize(sprintf(
            'O:%d:"%s"%s',
            strlen(__CLASS__),
            __CLASS__,
            strstr(strstr(serialize($obj), '"'), ':')
        ));
    }

    public function __toString() {
        $str = array();

        if (($y = $this->format("%y")) > 0)
            $str["y"] = sprintf("%d %s", $y, _n("year", "years", $y, GAWPP_TEXT_DOMAIN));

        if (($m = $this->format("%m")) > 0)
            $str["m"] = sprintf("%d %s", $m, _n("month", "months", $m, GAWPP_TEXT_DOMAIN));

        if (($d = $this->format("%d")) > 0)
            $str["d"] = sprintf("%d %s", $d, _n("day", "days", $d, GAWPP_TEXT_DOMAIN));

        if (empty($str) && ($h = $this->format("%h")) > 0)
            $str["h"] = sprintf("%d %s", $h, _n("hour", "hours", $h, GAWPP_TEXT_DOMAIN));

        if ((empty($str) || isset($str["h"])) && ($i = $this->format("%i")) > 0)
            $str["i"] = sprintf("%d %s", $i, _n("minute", "minutes", $i, GAWPP_TEXT_DOMAIN));

        if ((empty($str) || isset($str["i"])) && ($s = $this->format("%s")) > 0)
            $str["s"] = sprintf("%d %s", $s, _n("second", "seconds", $s, GAWPP_TEXT_DOMAIN));

        return join($str);
    }
}

class Gogs_API_Plugin_Widget extends WP_Widget {
    const DEFAULT_AMOUNT = 10;

    private static $types = array(
        "repos" => "Show repositories",
        "user" => "Show user profile"
    );

    public function __construct() {
        parent::__construct(
            "gawpp_widget",
            esc_html__("Gogs API WPPlugin", GAWPP_TEXT_DOMAIN),
            array(
                "classname" => "gawpp_widget",
                "description" => __("Gogs GIT", GAWPP_TEXT_DOMAIN),
            )
        );
    }

    public function widget($args, $instance) {
        wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
        wp_enqueue_style($this->name, plugins_url("_inc/widget.css", __FILE__));

        try {
            $user = Gogs_API_Plugin::user();

            switch($this->instance_get($instance, "type")) {
            case "repos":

                $limit = (int)$this->instance_get($instance, "amount", self::DEFAULT_AMOUNT);

                $i = 0;

                $out = array();
                $now = new DateTime();
                foreach($user->repos()->load()->all() as $key => $repo) {
                    $out[] = $this->partial("repo",
                        $repo->html_url,
                        $repo->name,
                        $repo->description,
                        $repo->stars_count,
                        $repo->forks_count,
                        !$repo->private ? " hidden" : "",
                        TimeAgo::set(
                            $now->diff(
                                new DateTime(!empty($repo->updated_at) ? $repo->updated_at : $repo->created_ad)
                            )
                        ) . $repo->updated_at
                    );
                }

                echo $this->view("repos", $args,
                    $this->instance_get($instance, "title", $args["widget_name"]),
                    join($out)
                );
                break;
            case "user":
                echo $user->username;
                break;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    private function partial(string $type, ...$args) {
        ob_start();
        include(GAWPP_DIR . "views/widget-" . $type . ".php");
        $page = ob_get_contents();
        ob_end_clean();

        $matches = array();

        $count = count($args);

        if ($matches_count = preg_match_all('/[\\%][a-z0-9][\\$a-z]{0,2}/s', $page, $matches)) {   

            $identicals = array();

            foreach($matches[0] as $match)
                $identicals[$match] = true;

            $matches_count = count($identicals);

            if ($count == $matches_count)
                return sprintf($page, ...$args);
        }

        return __CLASS__ . "::template::partial error (" . $matches_count . "!=" . $count .")";
    }

    public function view(string $type, array $tmpl_args, ...$args) {
        ob_start();
        include(GAWPP_DIR . "views/widget-" . $type . ".php");
        $page = ob_get_contents();
        ob_end_clean();

        $tmpl_args_arr = array(
            isset($tmpl_args["before_widget"]) ? $tmpl_args["before_widget"] : sprintf('<section id="%s" class="widget %s">', $this->id, $this->id_base),
            isset($tmpl_args["before_title"]) ? $tmpl_args["before_title"] : '<h2 class="widget-title">',
            isset($tmpl_args["after_title"]) ? $tmpl_args["after_title"] : '</h2>',
            isset($tmpl_args["after_widget"]) ? $tmpl_args["after_widget"] : '</section>'
        );

        $matches = array();

        $count = count($tmpl_args_arr) + count($args);

        if ($matches_count = preg_match_all('/[\\%][a-z0-9][\\$a-z]{0,2}/s', $page, $matches)) {   

            $identicals = array();

            foreach($matches[0] as $match)
                $identicals[$match] = true;

            $matches_count = count($identicals);

            if ($count == $matches_count)
                return sprintf($page, ...$tmpl_args_arr, ...$args);
        }

        return __CLASS__ . "::template error (" . $matches_count . "!=" . $count .")";
    }

    public function get_field_id($name) {
        return parent::get_field_id("widget_" . $name);
    }

    public function get_field_name($name) {
        return parent::get_field_name("widget_" . $name);
    }

    public function instance_get(array $instance, $name, $default = false) {
        return isset($instance["widget_" . $name]) ? $instance["widget_" . $name] : $default;
    }

    public function form($instance) {
        echo sprintf(
            '<p><label for="%1$s">%2$s</label><input class="widefat" type="text" id="%1$s" name="%3$s" placeholder="%4$s: %5$s"%6$s/></p>',
            esc_attr($this->get_field_id("title")),
            esc_attr__("Widget title:", GAWPP_TEXT_DOMAIN),
            esc_attr($this->get_field_name("title")),
            __("Default", GAWPP_TEXT_DOMAIN),
            esc_html($this->name),
            !empty($title = $this->instance_get($instance, "title", false)) ? (
                sprintf(' value="%s"', $title)
            ) : null
        );

        echo sprintf(
            '<p><label for="%1$s">%2$s</label><select class="widefat" id="%1$s" name="%3$s">%4$s</select></p>',
            esc_attr($this->get_field_id("type")),
            esc_attr__("Widget type:", GAWPP_TEXT_DOMAIN),
            esc_attr($this->get_field_name("type")),
            (function() use ($instance) {
                $opts = array();
                foreach (self::$types as $key => $type)
                    $opts[] = sprintf('<option value="%s"%s>%s</option>', 
                        $key, 
                        !$this->instance_get($instance, "type") || $this->instance_get($instance, "type") == $key ? " selected" : "",
                        $type
                    );

                return join($opts);
            })()
        );

        echo sprintf(
            '<p><label for="%1$s">%2$s</label><input class="widefat" type="number" id="%1$s" name="%3$s" value="%4$s" /></p>',
            esc_attr($this->get_field_id("amount")),
            esc_attr__("Amount:", GAWPP_TEXT_DOMAIN),
            esc_attr($this->get_field_name("amount")),
            esc_html($this->instance_get($instance, "amount", 10))
        );
    }
}       
